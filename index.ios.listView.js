/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ScrollView,
  WebView,
  ListView,
  Navigator,
  TouchableHighlight,
  View
} from 'react-native';
import NavBar, { NavButton, NavButtonText } from 'react-native-nav'; 

class MyDataMenu extends Component {
	render() {
		return (
			<ScrollView>
				<DataButton item={this.props.dataRow}/>
			</ScrollView>
		);
	}
}
class DataButton extends Component{
	showContent(item){
		console.log(item.title);
		// return <ContentView link={item.link} title={item.title}/>
		return <Navigator 
			style={styles.container}
			initialRoute={{ title: item.title }}
			renderScene={(route, nav)=> <ContentView 
				item={item}
				navigator={nav}
				route={route}
			/>}
		/>
	}
	render(){
		return(
			<TouchableHighlight onPress={()=>this.showContent(this.props.item)}>
				<View style={styles.button} >
					<Text>{this.props.item.title}</Text>
				</View>
			</TouchableHighlight>
		);
	}
}
class ContentView extends Component {
	render() {
		return (
			<ScrollView>
				<NavBar>{this.props.item.title}</NavBar>
				<WebView source={{
					uri: 'http://cnodejs.org'+this.props.item.link
				}} style={{ marginTop : 20 }}/>
			</ScrollView>
		);
	}
}

export default class testNavigator extends Component {
	constructor(props, context) {
		super(props, context);
		const ds = new ListView.DataSource({ rowHasChanged : (r1, r2)=> r1 !== r2 }); 
		this.state = {
			dataList : ds.cloneWithRows([])
		};
		this.AjaxGetData();
		////setInterval(this.AjaxGetData,8000);
	}
	AjaxGetData(){
		fetch('http://192.168.2.110:3000')
		.then((response) => response.json())
		.then((responseData)=>{
			//console.log(responseData);
			this.setState({
				dataList : this.state.dataList.cloneWithRows( responseData )
			});
		})
		.done();
	}
	_renderRow(dataRow){
		return <MyDataMenu dataRow={dataRow}/>
	}
  	render() {
    	return (
			<View>
				<NavBar></NavBar>
				<ListView 
					dataSource={this.state.dataList}
					renderRow={this._renderRow.bind(this)}
					enableEmptySections={true}
				/>
			</View>
    	);
  	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
	//padding : 30
	//marginTop : 20
  },
  button: {
    backgroundColor: 'white',
    padding: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#CDCDCD',
  },
});

AppRegistry.registerComponent('testNavigator', () => testNavigator);
