
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ScrollView,
  WebView,
  Navigator,
  TouchableHighlight,
  View
} from 'react-native';
import NavBar, { NavButton, NavButtonText } from 'react-native-nav'; 

class MyDataMenu extends Component {
	render() {
		let buttonRow = [];
		if( this.props.dataRow ){
			this.props.dataRow.map((item, index)=>{
				buttonRow.push(<DataButton item={item} key={index} navigator={this.props.navigator}/>);
			});
		}
		return (
			<ScrollView>
				<NavBar>{this.props.title}</NavBar>
				{buttonRow}
			</ScrollView>
		);
	}
}
class DataButton extends Component{
	showContent(item){
		this.props.navigator.push(item);
	}
	render(){
		return(
			<TouchableHighlight onPress={()=>this.showContent(this.props.item)}>
				<View style={styles.button} >
					<Text>{this.props.item.title}</Text>
				</View>
			</TouchableHighlight>
		);
	}
}
class ContentView extends Component {
	render() {
		let nowLink  = 'http://cnodejs.org'+this.props.route.link;
		// let nowLink  = 'http://demo.app';
		// let nowLink = 'https://shop.shiatzychen.com';
		return (
			<WebView
				source={{uri: nowLink}}
				style={{marginTop: 20}}
			/>
		);
	}
}

export default class testNavigator extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			dataList : []
		};
		this.AjaxGetData();
		//setInterval(this.AjaxGetData,3000);
	}
	AjaxGetData(){
		fetch('http://localhost:3000')
		.then((response) => response.json())
		.then((responseData)=>{
			this.setState({
				dataList : responseData
			});
		});
	}
  	render() {
    	return (
			<Navigator 
				style={styles.container}
				initialRoute={{ title:'home' }}
				renderScene={(route, nav)=> {
					if( route.title == 'home' ){
						return <MyDataMenu 
							dataRow={this.state.dataList}
							navigator={nav}
							route={route}
						/>
					}else{
						return <ContentView route={route} />
					}

				}}
			/>
    	);
  	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
	//padding : 30
	marginTop : 20
  },
  button: {
    backgroundColor: 'white',
    padding: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#CDCDCD',
  },
});

AppRegistry.registerComponent('testNavigator', () => testNavigator);
